/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.midtermq1;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Vending {

    private int money;
    private int number;
    private int Balance;
    String drinks;
    int total;
    Scanner kb = new Scanner(System.in);

    public Vending() {
        this.money = money;
        this.number = number;

    }

    public void showVending() {
        System.out.println("-------------------");
        System.out.println(">!<!>Vending Machine KARN<!>!<");
        System.out.println("Choose the number water want");
        System.out.println("1.Water" + "\n2.Coca-Cola" + "\n3.Coffee"
                + "\n4.Matcha Tea" + "\n5.Milk Tea");
        System.out.println("-------------------");

    }

    public void inputMoney() {
        System.out.println("Please drop money : ");
        money = kb.nextInt();

    }

    public void inputNumber() {
        System.out.println("Please Choose Number of water: ");
        number = kb.nextInt();

    }

    public boolean choose(int number) {
        switch (number) {
            case 1:
                if (total >= 7) {
                    drinks = "Water";
                    Balance = total - 7;
                    print();
                }
                break;
            case 2:
                if (total >= 15) {
                    Balance = total - 15;
                    drinks = "Coca-Cola";
                    print();
                } else {
                    System.out.println("You can not choose Coca-cola");
                    inputMoneyAgain();
                }
                break;
            case 3:
                if (total >= 20) {
                    Balance = total - 20;
                    drinks = "Coffee";
                    print();
                } else {
                    System.out.println("You can not choose Coffee");
                    inputMoneyAgain();
                }
                break;
            case 4:
                if (total >= 25) {
                    Balance = total - 25;
                    drinks = "Matcha Tea";
                    print();
                } else {
                    System.out.println("You can not choose Matcha Tea");
                    inputMoneyAgain();
                }
                break;
            case 5:
                if (total >= 30) {
                    Balance = total - 30;
                    drinks = "Milk Tea";
                    print();
                } else {
                    System.out.println("You can not choose Milk Tea");
                    inputMoneyAgain();
                }
                break;

        }
        return true;
    }

    public void print() {
        System.out.println(drinks + " be yours!!!" + "Balance : " + Balance);

    }

    public boolean checkMoney() {
        if (total < 7) {
            System.out.println("The money not enough!!!");
        } else {
            return true;
        }
        return false;
    }

    public void run() {
        this.showVending();
        this.inputMoney();
        total += money;
        System.out.println("money: " + total);
        if (this.checkMoney() == true) {
            this.inputNumber();
            this.choose(number);
        } else {
            inputMoneyAgain();

        }

    }

    public void inputMoneyAgain() {
        this.showVending();
        System.out.println("***Add more money again please***");
        this.inputMoney();
        total += money;
        System.out.println("money: " + total);

        if (this.checkMoney() == true) {
            this.inputNumber();
            this.choose(number);
        }
    }

}
